# @widgitlabs/stylelint-config

Widgit Labs coding standards for CSS and SCSS.

## Installation

This package is a stylelint shareable configuration, and requires the `stylelint` library.

To install this config and dependencies:

```bash
npm install --save-dev stylelint @widgitlabs/stylelint-config
```

Then, add a `.stylelintrc` file and extend these rules. You can also add your own rules and overrides for further customization.

```json
{
  "extends": "@widgitlabs/stylelint-config",
  "rules": {
    ...
  }
}
```
