<?php
/**
 * Functions declared outside of `namespace.php` should generate a warning.
 */

namespace WidgitLabs\Coding\Standards\Test;

/**
 * This function should be in namespace.php instead.
 */
function foo() {}
