# Widgit Labs Coding Standards

[![License: GPL-3.0+](https://img.shields.io/badge/license-GPL--3.0%2B-blue.svg)](https://gitlab.com/widgitlabs/wlcs/blob/master/license.txt)
[![Pipelines](https://gitlab.com/widgitlabs/wlcs/badges/master/pipeline.svg)](https://gitlab.com/widgitlabs/wlcs/pipelines)
[![Discord](https://img.shields.io/discord/586467473503813633?color=899AF9)](https://discord.gg/jrydFBP)

This is a codified version of the [Widgit Labs style guide](https://handbook.widgitlabs.com/). We include phpcs, ESLint, and stylelint rules.

## Contributing

We welcome contributions to these standards and want to make the experience as seamless as possible. To learn more about contributing, please reference the [CONTRIBUTING.md](https://gitlab.com/widgitlabs/wlcs/-/blob/master/CONTRIBUTING.md) file.

## Setup

Each ruleset is available individually via Composer or NPM. To install the needed ruleset, use one of the following commands:

 - PHPCS: `composer require --dev widgitlabs/wlcs`
 - ESLint: `npx install-peerdeps --dev @widgitlabs/eslint-config@latest`
 - stylelint: `npm install --save-dev stylelint @widgitlabs/stylelint-config`

## Using PHPCS

Run the following command to run the standards checks:

```
vendor/bin/phpcs --standard=vendor/widgitlabs/wlcs .
```

We use the [DealerDirect phpcodesniffer-composer-installer](https://github.com/Dealerdirect/phpcodesniffer-composer-installer) package to handle `installed_paths` for PHPCS when first installing the Widgit Labs ruleset. If you an error such as `ERROR: Referenced sniff "WordPress-Core" does not exist`, delete the `composer.lock` file and `vendor` directories and re-install Composer dependencies.   

The final `.` here specifies the files you want to test; this is typically the current directory (`.`), but you can also selectively check files or directories by specifying them instead.

You can add this to your Travis YAML file as a test:

```yaml
script:
  - phpunit
  - vendor/bin/phpcs --standard=vendor/widgitlabs/wlcs .
```

### Excluding Files

This standard includes special support for a `.phpcsignore` file (in the future, this should be [built into phpcs itself](https://github.com/squizlabs/PHP_CodeSniffer/issues/1884)). Simply place a `.phpcsignore` file in your root directory (wherever you're going to run `phpcs` from).

The format of this file is similar to `.gitignore` and similar files: one pattern per line, comment lines should start with a `#`, and whitespace-only lines are ignored:

```
# Exclude our tests directory.
tests/

# Exclude any file ending with ".inc"
*\.inc
```

Note that the patterns should match [the PHP_CodeSniffer style](https://github.com/squizlabs/PHP_CodeSniffer/wiki/Advanced-Usage#ignoring-files-and-folders): `*` is translated to `.*` for convenience, but all other characters work like a regular expression.

Patterns are relative to the directory that the `.phpcsignore` file lives in. On load, they are translated to absolute patterns: e.g. `*/tests/*` in `/your/dir/.phpcsignore` will become `/your/dir/.*/tests/.*` as a regular expression. **This differs from the regular PHP_CodeSniffer practice.**


### Advanced/Extending

If you want to add further rules (such as WordPress.com VIP-specific rules) or customize PHPCS defaults, you can create your own custom standard file (e.g. `phpcs.ruleset.xml`):

```xml
<?xml version="1.0"?>
<ruleset>
	<!-- Files or directories to check -->
	<file>.</file>

	<!-- Path to strip from the front of file paths inside reports (displays shorter paths) -->
	<arg name="basepath" value="." />

	<!-- Set a minimum PHP version for PHPCompatibility -->
	<config name="testVersion" value="7.2-" />

	<!-- Use Widgit Labs Coding Standards -->
	<rule ref="vendor/widgitlabs/wlcs" />

	<!-- Add VIP-specific rules -->
	<rule ref="WordPress-VIP" />
</ruleset>
```

You can then reference this file when running phpcs:

```
vendor/bin/phpcs --standard=phpcs.ruleset.xml .
```


#### Excluding/Disabling Checks

You can also customise the rule to exclude elements if they aren't applicable to the project:

```xml
<rule ref="vendor/widgitlabs/wlcs">
	<!-- Disable ESLint -->
	<exclude name="WidgitLabs.Debug.ESLintSniff" />
</rule>
```

Rules can also be disabled inline. [phpcs rules can be disabled](https://github.com/squizlabs/PHP_CodeSniffer/wiki/Advanced-Usage#ignoring-parts-of-a-file) with a `// @codingStandardsIgnoreLine` comment, and [ESLint rules can be disabled](http://eslint.org/docs/user-guide/configuring#disabling-rules-with-inline-comments) with a `/* eslint disable ... */` comment.

To find out what these codes are, specify `-s` when running `phpcs`, and the code will be output as well. You can specify a full code, or a partial one to disable groups of errors.


## Included Checks

The phpcs standard is based upon the `WordPress-VIP` standard from [WordPress Coding Standards](https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards), with [customisation and additions](https://gitlab.com/widgitlabs/wlcs/-/blob/master/WidgitLabs/ruleset.xml) to match our style guide.

## Using ESLint

The ESLint package contains an [ESLint](https://eslint.org/) configuration which you can use to validate your JavaScript code style. While it is possible to run ESLint via phpcs, we recommend you install and use eslint via npm directly. See [the `@widgitlabs/eslint-config` package README](https://gitlab.com/widgitlabs/wlcs/-/blob/master/packages/eslint-config-widgitlabs/readme.md) for more information on configuring ESLint to use the Widgit Labs coding standards.

Once you have installed the [`@widgitlabs/eslint-config` npm package](https://www.npmjs.com/package/@widgitlabs/eslint-config), you may simply specify that your own project-level ESLint file extends the `widgitlabs` configuration. If you install this globally (`npm install -g @widgitlabs/eslint-config`) you can also reference the configuration directly from the command line via `eslint -c widgitlabs .`

Alternatively, you can create your own configuration and extend these rules:

`.eslintrc`
```json
{
  "extends": "@widgitlabs"
}
```

## Using stylelint

The stylelint package contains a [stylelint](https://stylelint.io/) configuration which you can use to validate your CSS and SCSS code style. We recommend you install and use stylelint via npm directly. See [the `@widgitlabs/stylelint` package README](https://gitlab.com/widgitlabs/wlcs/-/blob/master/packages/stylelint-config/readme.md) for more information on configuring stylelint to use the Widgit Labs coding standards.

To integrate the Widgit Labs rules into your project, add a `.stylelintrc` file and extend these rules. You can also add your own rules and overrides for further customization.

```json
{
  "extends": "@widgitlabs/stylelint-config",
  "rules": {
    ...
  }
}
```
